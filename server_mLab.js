require('dotenv').config();
// request-json : permite hacer una peticion desde nodejs
const request_json = require('request-json');
const express = require('express');
const body_parser = require('body-parser');
const app = express();// importa del nodejs;
const port = process.env.PORT || 3001; //process.env obtiene la variable de entorno del servidor
const URL_BASE = '/techu/v2/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu51db/collections/';
//la variable del apikey debe ir en el archivo .env por seguridad
//const apikey_mlab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikey_mlab = 'apiKey=' + process.env.apiKey;
const http_client =request_json.createClient(URL_MLAB);
const cors = require('cors');
var parseInt = require('parse-int');
//const usersFiles = require('./user.json');

app.listen(port, function () {
  console.log('Escuchando desde el puerto '+ port);
});

app.use(cors());
app.options('*', cors());

app.use(body_parser.json());
// EL ARCHIVO server_mLab.js contiene la comunicacion del backend con MLAB
//Metodo GET - para obtener users - LISTADO DE USUARIOS
app.get(URL_BASE + 'users',
  function (req,res) {
        console.log('Get de USER -  LISTADO DE USUARIOS');
        let field = 'f={"_id":0}&'
        http_client.get('user?'+ field + apikey_mlab,
          function (error, responseMlab, body) { // body traer el objeto del msj http
            //  console.log("Error" + error);
            //  console.log("Respuesta Mlab" + responseMlab);
            //  console.log('body' + body);
            //  response.send(body); // va response porque tiene devolver la repsuesta al localhost
              var response = {};
              if (error) {
                response = {"msg" : "Error al recuperar users de mLab."};
                res.status(500);
              } else {
                if (body.length > 0) {
                  response = body;
                } else {
                  response = {"msg" : "Usuario no encontrado."};
                  res.status(400);
                }
              }
              res.send(response);
          });
      //response.status(200).send({'msg':'Get Cliente HTTP con MLAB'}); //la respuesta send se envia al final
  });

// GET con ID usando el MLAB - CONSULTA DE UN USUARIO
app.get(URL_BASE + 'users/:id',
  function(req, res){
    console.log("Get user con ID - CONSULTA DE UN USUARIO");
    let id = req.params.id;
    let queryString = 'q={"id_user":'+id+'}&';
    let field = 'f={"_id":0}&';
    http_client.get('user?'+field+queryString+apikey_mlab,
      function (error, res_body, body) {
        //console.log("Error" + error);
        //console.log("Respuesta Mlab" + res_body);
        //console.log('body' + body);
        let response = {};
        if (error) {
          response = {"msg" : "Error al recuperar users de mLab."};
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});

//GET cuentas de un usuario obtener del MLAB -  CONSULTA DE CUENTAS DE UN USUARIO
//app.get(URL_BASE +'users/:id',
app.get(URL_BASE +'users/:id/accounts',
  function (req,res) {
      console.log("GET de Cuentas con ID usuario -  CONSULTA DE CUENTAS DE UN USUARIO");
      //const http_client =request_json.createClient(URL_MLAB);
      let id = req.params.id;
      let queryString = 'q={"id_user":'+id+'}&';
    //  let field = 'f={"_id":0}&';
      let ctas = 'f={"account":1, "_id":0}&';
      http_client.get('user?'+queryString +ctas+ apikey_mlab,
        function (error, res_body, body) {
          //console.log("Error" + error);
          //console.log("Respuesta Mlab" + res_body);
          //console.log(`Respuesta Mlab: ${JSON.stringify(res_body)}`);
          //console.log(`body: + body );
          //console.log(`body: ${JSON.stringify(body)}`);
          var response = {};
          if (error) {
            response = {"msg" : "Error al recuperar users de mLab."};
            res.status(500);
          } else {
            if (body.length > 0) {
              if (JSON.stringify(body[0]) ==='{}') {
                  response = {"msg" : "Usuario sin cuentas"};
              }else {
                  const ctasnofilter = body[0].account;
                  let logctasle = body[0].account.length;
                  var ctasActs2 = [];
                  for (let i = 0; i < logctasle; i++) {
                       if (ctasnofilter[i].estado == "A") {
                         ctasActs2.push(ctasnofilter[i]);
                      }
                  }
                  var ctasacts = [{ account: ctasActs2}];
                  response = ctasacts;
                  //response = body;
              }
            } else {
              response = {"msg" : "Usuario no encontrado."};
              res.status(404);
            }
          }
          res.send(response);
        });

  });

//peticiòn POST a users con MLAB
//CREACION DE UN NUEVO USUARIO (SOLO DATOS DEL USUARIO )
app.post(URL_BASE + 'users',
  function(req, res){
  console.log('POST de User - CREACION NUEVO USUARIO');
  let queryString = 'c=true&';
  http_client.get ('user?'+queryString +apikey_mlab,
    function (error, res_mlab, body_mlab) {
    let newID = body_mlab + 1 ;
    //console.log(body_mlab);
    //console.log(newID);
    let newUser ={
      "id_user": newID,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password,
      "direccion": req.body.direccion,
      "telefono": req.body.telefono
    };
    //console.log(newUser);
    http_client.post('user?'+apikey_mlab, newUser,
      function (error, res_mlab, bodyPost) {
        //console.log(bodyPost);
        res.send(bodyPost);
      });
    });
});

//peticiòn PUT a users con MLAB con campo ID
//ACTUALIZACION DE UN USUARIO CON ID
app.put(URL_BASE + 'usersUpd/:id',
  function(req, res){
    console.log('PUT de users ' + req.params.id+' - ACTUALIZACION DE UN USUARIO CON ID');
    let id = req.params.id;
    let queryId = 'q={"id_user":'+id+'}&';
    //const http_client =request_json.createClient(URL_MLAB);
    http_client.get('user?'+queryId+apikey_mlab,
      function (error,res_mlab,body) {
        var cambio = '{"$set":'+JSON.stringify(req.body)+'}';
      //  console.log(req.body);
      //  console.log(cambio);
        http_client.put('user?'+queryId+apikey_mlab, JSON.parse(cambio),
        function (error,res_mlab,body) {
        //  console.log("body:" + body);
          res.send("Usuario Modificado" + JSON.stringify(body));
        });
      });
});

//DELETE de un Usuario con MLAB
//ELIMINAR USUARIO CON ID
app.delete(URL_BASE + 'usersDel/:id',
  function (req,res) {
    console.log('Delete de user ' + req.params.id + " ELIMINAR UN USUARIO CON ID");
    let id = req.params.id;
    let queryId = 'q={"id_user":'+id+'}&';
  //  const http_client = request_json.createClient(URL_MLAB);
    http_client.get('user?'+queryId+apikey_mlab,
      function (error, res_mlab, body) {
        let respuesta = body[0];
        //console.log(respuesta);
        //console.log("Body delete:"+respuesta);
        // la busqueda del usurio es direcata a travezrespuesta._id.$oid
        http_client.delete('user/'+respuesta._id.$oid+'?'+apikey_mlab,
          function (error,res_mlab,body) {
            res.send(body);
        });
      });
});

//Method POST login con MLAB
app.post(URL_BASE + "login",
  function (req, res){
    console.log("LOGIN con Email y Password");
    let email = req.body.email;
    let password = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + password + '"}&';
    let limFilter = 'l=1&';
    //let http_client = request_json.createClient(URL_MLAB);
    http_client.get('user?'+ queryString + limFilter + apikey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            //let session={"logged":true};
            //let login ='{"$set":'+JSON.stringify(session)+'}';
            let login = '{"$set":{"logged":true}}';
            http_client.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                //console.log(login);
                res.send({'msg':'Login correcto',
                          'user':body[0].email,
                          'userid':body[0].id_user,
                          'name':body[0].first_name,
                          'last':body[0].last_name,
                          'direccion': body[0].direccion,
                          'telefono': body[0].telefono
                        });
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout Con MLAB
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("LOGOUT con Email");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    //var  http_client = request_json.createClient(URL_MLAB);
    http_client.get('user?'+ queryString + apikey_mlab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            http_client.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email, 'first_name':respuesta.first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});



//CREACION DE CUENTAS PARA USUARIO -MLAB
app.put(URL_BASE + 'users/:id/account',
  function (req,res) {
    console.log("Crear cuenta a user "+ req.params.id+ " CREAR CUENTA A USUARIO");
    let id = req.params.id;
    let queryString = 'q={"id_user":'+id+'}&';
    let ctas = 'f={"account":1, "_id":0}&';
    http_client.get('user?'+queryString +ctas+ apikey_mlab,
    function (error, res_body, body_mlab) {
    //  console.log(`body: ${JSON.stringify(body_mlab)}`);
      let ctasmlab = body_mlab[0];
      if (JSON.stringify(body_mlab[0]) ==='{}'){
        let newIdcta = 1;
        let posic = newIdcta -1;
        var newCta = {
            "account":{
              "id_account": newIdcta,
              "iban": req.body.iban,
              "saldo": req.body.saldo,
              "divisa": req.body.divisa,
              "tip_cuenta": req.body.tip_cuenta,
              "estado": req.body.estado
            }
        };
        //console.log(newCta);
        var cambio = '{"$addToSet":'+JSON.stringify(newCta)+'}';
      //  var cambio ={"$addToSet": {"account":{"id_account":1,"iban":"0011-0199","saldo":10.00}}}
      }else {
        let newIdcta = ctasmlab.account.length + 1;
        let posic = newIdcta -1;
        let accPos1 = 'account.'+posic+'.id_account';
        let accPos2 = 'account.'+posic+'.iban';
        let accPos3 = 'account.'+posic+'.saldo';
        let accPos4 = 'account.'+posic+'.divisa';
        let accPos5 = 'account.'+posic+'.tip_cuenta';
        let accPos6 = 'account.'+posic+'.estado';
        var newCta = {
              [accPos1]: newIdcta,
              [accPos2]: req.body.iban,
              [accPos3]: req.body.saldo,
              [accPos4]: req.body.divisa,
              [accPos5]: req.body.tip_cuenta,
              [accPos6]: req.body.estado
        };
        var cambio = '{"$set":'+JSON.stringify(newCta)+'}';
      }
      http_client.put('user?'+queryString+apikey_mlab, JSON.parse(cambio),
      function (error,res_mlab,body_mlab) {
        //console.log("body:" + body_mlab);
        res.send("Cuenta creada " + JSON.stringify(body_mlab));
      });
    });
  });

//Funcionalidad CUSTOM
//INACTIVAR CUENTA DE UN USUARIO
app.put(URL_BASE + 'users/:id/accountInc/:idacc',
  function (req, res) {
    console.log("INACTIVA CTA usuario "+req.params.id+" - ID CTA:"+req.params.idacc) ;
    let id = req.params.id;
    let idacc = req.params.idacc;
    let idcta = parseInt(idacc);
    let queryString = 'q={"id_user":'+id+'}&';
    let ctas = 'f={"account":1, "_id":0}&';
    http_client.get('user?'+queryString +ctas+ apikey_mlab,
    function (error, res_body, body_mlab) {
      //console.log(body_mlab[0].account);
      var arrayctas = body_mlab[0].account;
      const arrLong = arrayctas.length;
      var i;
      function encuentraAtrib(array,attr,value){
        for(i=0; i<arrLong; i++){
          var condicion = array[i];
          var conidcta = condicion.id_account;
	         if(conidcta == idacc) {
              return i;
           }
              // return -1;
          }
        }
        var indicex = encuentraAtrib(arrayctas, 'id_account', idacc);
        let accPos1 = 'account.'+indicex+'.estado';
        var newEst = {
              [accPos1]: 'I'
        };
        var cambio = '{"$set":'+JSON.stringify(newEst)+'}';
       //console.log(cambio);
       http_client.put('user?'+queryString+apikey_mlab, JSON.parse(cambio),
       function (error,res_mlab,body_mlab) {
//        console.log("body:" + body_mlab);
        res.send("Cuenta Inactiva " + JSON.stringify(body_mlab));
      });
    });
  });

//Actualizar Datos de una Cuenta
//Actualiza Saldo de una Cuenta
app.put(URL_BASE + 'users/:id/accountSal/:idacc',
  function (req, res) {
    console.log("ACTUALIZA SALDO CTA usuario "+req.params.id+" - ID CTA:"+req.params.idacc) ;
    let id = req.params.id;
    let idacc = req.params.idacc;
    let idcta = parseInt(idacc);
    let queryString = 'q={"id_user":'+id+'}&';
    let ctas = 'f={"account":1, "_id":0}&';
    http_client.get('user?'+queryString +ctas+ apikey_mlab,
    function (error, res_body, body_mlab) {
      //console.log(body_mlab[0].account);
      var arrayctas = body_mlab[0].account;
      const arrLong = arrayctas.length;
      var i;
      function encuentraAtrib(array,attr,value){
        for(i=0; i<arrLong; i++){
          var condicion = array[i];
          //console.log(condicion);
          var conidcta = condicion.id_account;
	         if(conidcta == idacc) {
              return i;
           }
            	//console.log("Nada");
              // return -1;
          }
        }
        var indicex = encuentraAtrib(arrayctas, 'id_account', idacc);
        let accPos1 = 'account.'+indicex+'.saldo';
        var newSal = {[accPos1]: req.body.saldo};
        var cambio = '{"$set":'+JSON.stringify(newSal)+'}';
        http_client.put('user?'+queryString+apikey_mlab, JSON.parse(cambio),
       function (error,res_mlab,body_mlab) {
//        console.log("body:" + body_mlab);
         res.send("Saldo Actualizado " + JSON.stringify(body_mlab));
       });
    });
  });

  //OPERACIONES CON AL TABLA MOVIMIENTO                      --> MODIFICADO
  //GET a la tabla de Movimiento - Listado por cliente
  app.get(URL_BASE + 'accounts/:id',
    function(req, res){
      console.log("Listado de movimiento de una cuenta "+ req.params.id);
      let id = JSON.stringify(req.params.id);
      let queryString = `q={"id_cuenta": ${id} }&`;
      let fieldString = 'f={"_id":0, "referencia_mov":0}&';
      http_client.get(`movimiento?${queryString}&${fieldString}&${apikey_mlab}`,
        function(error, res_mlab, cuerpo){
          let response = {};
          if(error) {
              console.log(`Error: ' ${error}`);
              response = {"msg" : "Error en la peticion mLab."}
              res.status(500);
          } else {
            if(cuerpo.length > 0) {
              //console.log(`Cuerpo: ' ${JSON.stringify(cuerpo)}`);
              response = cuerpo;
            } else {
              response = {"msg" : "El numero de cuenta " + id + " no existe."};
              res.status(404);
            }
          }
          res.send(response);
        });
  });
  //GET a la tabla de Movimiento - Detalle de un movimiento  --> MODIFICADO
  app.get(URL_BASE + 'accounts/:id/movimientos/:id_mov',
        function(req, res){
          console.log("Detalle de un movimiento "+ req.params.id_mov+ "  -cuenta: "+req.params.id);
          let id = JSON.stringify(req.params.id);
          let id_mov = req.params.id_mov;
          let queryString = `q={"id_cuenta": ${id}, "id_movimiento": ${id_mov} }&`;
          let fieldString = 'f={"_id":0 }&';
          http_client.get(`movimiento?${queryString}&${fieldString}&${apikey_mlab}`,
            function(error, res_mlab, cuerpo){
              let response = {};
              if(error) {
                  console.log(`Error: ' ${error}`);
                  response = {"msg" : "Error en la peticion mLab."}
                  res.status(500);
              } else {
                if(cuerpo.length > 0) {
                  //console.log(`Cuerpo: ' ${JSON.stringify(cuerpo)}`);
                  response = cuerpo;
                } else {
                  response = {"msg" : "El numero de movimiento " + id_mov + " no existe."};
                  res.status(404);
                }
              }
              res.send(response);
            });
    });
  //DELETE a la tabla de Movimiento                         --> MODIFICADO
  app.delete(URL_BASE + 'accounts/:id/movimientos/:id_mov',
    function(req, res){
      console.log("Eliminar movimiento " + req.params.id+ "   -cuenta: " +req.params.id_mov);
      let id = JSON.stringify(req.params.id);
      let id_mov = req.params.id_mov;
      let queryString = `q={"id_cuenta": ${id}, "id_movimiento": ${id_mov} }&`;
      http_client.get(`movimiento?${queryString}${apikey_mlab}`,
        function(error, respuestaMLab, body){
          let respuesta = body[0];
          let response = {};
          //console.log("Body delete:"+ JSON.stringify(respuesta));
          if(body.length > 0) {
              http_client.delete("movimiento/" + respuesta._id.$oid +'?'+ apikey_mlab,
                function(error, respuestaMLab, cuerpo){
                  if(error) {
                      //console.log(`Error: ' ${error}`);
                      response = {"msg" : "Error en la peticion mLab."}
                      res.status(500);
                  } else {
                    response = {"msg" : "El numero de movimiento " + id_mov + " eliminado."};
                    //console.log("cuerpo: " + JSON.stringify(cuerpo));
                    res.status(200);
                  }
                  res.send(response);
              });
            } else {
              response = {"msg" : "El numero de movimiento " + id_mov + " a eliminar no existe."};
              res.status(404);
              res.send(response);
            }
        });
    });
  //POST a la tabla de Movimiento, id_cuenta como parte de la URL --> MODIFICADO
  app.post(URL_BASE + 'accounts/:id',
      function(req, res) {
        console.log("Insertar movimiento en la cuenta "+req.params.id);
        let count_param = 'c=true';
        let id = JSON.stringify(req.params.id);
        let queryString = `q={"id_cuenta": ${id} }&`;
        http_client.get(`movimiento?${queryString}&${count_param}&${apikey_mlab}`,
          function(error, respuestaMLab , cantidad) {
            let newID = cantidad + 1;
            var newMovi = {
              "id_cuenta" : req.params.id,
              "id_movimiento" : newID,
              "importe_mov" : req.body.importe_mov,
              "moneda_mov" : req.body.moneda_mov,
              "date_mov" : req.body.date_mov,
              "referencia_mov" : req.body.referencia_mov,
            };
            //console.log("Insertar:" + JSON.stringify(newMovi));
            http_client.post(`movimiento?&${apikey_mlab}`, newMovi ,
             function(error, respuestaMLab, body) {
              res.status(201).send(body);
           });
        });
  });
  //POST a la tabla de Movimiento, id_cuenta como parte del Body  --> MODIFICADO
  app.post(URL_BASE + 'accounts',
      function(req, res) {
        console.log("Insertar un nuevo movimiento en la cuenta "+req.body.id_cuenta);
        let count_param = 'c=true';
        let id = JSON.stringify(req.body.id_cuenta);
        let queryString = `q={"id_cuenta": ${id} }&`;
        http_client.get(`movimiento?${queryString}&${count_param}&${apikey_mlab}`,
          function(error, respuestaMLab , count) {
            let newID = count + 1;
            var newMovi = {
              "id_cuenta" : req.body.id_cuenta,
              "id_movimiento" : newID,
              "importe_mov" : req.body.importe_mov,
              "moneda_mov" : req.body.moneda_mov,
              "date_mov" : req.body.date_mov,
              "referencia_mov" : req.body.referencia_mov,
            };
            //console.log("Insertar:" + JSON.stringify(newMovi));
            http_client.post(`movimiento?&${apikey_mlab}`, newMovi ,
             function(error, respuestaMLab, body) {
              res.status(201).send(body);
           });
        });
    });
//Devolver la cantidad de usuario registrados
app.get(URL_BASE + 'total_users',
    function(request, response){
      let tam = usersFiles.length;
      console.log('Numero de usuarios: ' + tam);
      response.status(200).send({"num_usuarios " : tam});
});
