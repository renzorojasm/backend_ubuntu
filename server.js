const express = require('express');
const body_parser = require('body-parser');
const app = express();// importa del nodejs;
const port = process.env.PORT || 3000; //process.env obtiene la variable de entorno del servidor
const URL_BASE = '/techu/v1/';
const usersFiles = require('./user.json');

app.listen(port, function () {
  console.log('Escuchando desde el puerto 3000');
});

app.use(body_parser.json());

//Metodo GET - para obtener users
app.get(URL_BASE + 'users',
  function (req,res) {
      console.log('Consulta Get User');
//      res.send('Hola Peru');
//      response.send({'usuario':'Renzo Rojas'});
        res.status(200).send(usersFiles); //la respuesta send se envia al final
  }
);

// Peticiòn Get a un unico usuario mediante su id (instancia)
app.get(URL_BASE + 'users/:id',
  function(request, response){
    console.log(request.params.id);
    let pos = request.params.id - 1;
    let respuesta =
         (usersFiles[pos] == undefined) ? {"msg":"usuario no existente"} : usersFiles[pos];
    let status_code = (usersFiles[pos] == undefined) ? 404 : 200
//    response.send(usersFiles[pos]);
    response.status(status_code).send(respuesta);
});

//uso de mas params
app.get(URL_BASE + 'users/:id/:p1/:p2',
  function(request, response){
    console.log('id: ' + request.params.id);
    console.log('p1: ' + request.params.p1);
    console.log('p2: ' + request.params.p2);
    let pos = request.params.id - 1;
    let respuesta =
         (usersFiles[pos] == undefined) ? {"msg":"usuario no existente"} : usersFiles[pos];
    let status_code = (usersFiles[pos] == undefined) ? 404 : 200
    response.status(status_code).send(respuesta);
});

//GET con query
app.get(URL_BASE + 'users1',
    function(req, res){
      console.log(req.query.id);
      console.log(req.query.country);
      res.status(200).send({"msg":"Es un GET con query"});
});

// Petición GET con Query String (req.query)
app.get(URL_BASE + 'users2',
  function(req, res) {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    let pos = req.query.id - 1;
    let respuesta =
         (usersFiles[pos] == undefined) ? {"msg":"Usuario no existente"} : usersFiles[pos];
    let status_code = (usersFiles[pos] == undefined) ? 404 : 200
    res.status(status_code).send(respuesta);
    res.send({"msg" : "GET con query string"});
});


//peticiòn POST a users
app.post(URL_BASE + 'users',
  function(req, res){
    console.log('POST a users');
    let tam = usersFiles.length;
    let new_user = {
      "id_user": tam + 1,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password
    }
    console.log(new_user);
    usersFiles.push(new_user);
    res.status(201).send({"msg":"Usuario creado correctamente"});
});

//peticiòn PUT a users
app.put(URL_BASE + 'users/:id',
  function(req, res){
    console.log('PUT a users ' + req.params.id);
    let pos = req.params.id - 1;
    let user = usersFiles[pos];
    let respuesta, status, mod_user;
    if(user == undefined){
      respuesta = {"msg":"Usuario no existe"};
      status = 404;
    }else{
      mod_user = {
        "id_user": pos + 1,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password
      }
      respuesta = {"msg":"Usuario modificado correctamente" + pos};
      status = 200;
    }
    console.log(mod_user);
    usersFiles[pos] = mod_user;
    res.status(status).send(respuesta);
});

//PUT del ejemplo de clase
app.put(URL_BASE + 'users1/:id',
  function(req, res){
     console.log("PUT /techu/v1/users/:id " + req.params.id);
     let idBuscar = req.params.id;
     let updateUser = req.body;
     for(let i = 0; i < usersFiles.length; i++) {
       console.log(usersFiles[i].id_user);
       if(usersFiles[i].id_user == idBuscar) {
         usersFiles[i] = updateUser;
         res.status(200).send({"msg" : "Usuario actualizado correctamente.", updateUser});
        }
      }
      res.status(404).send({"msg" : "Usuario noo encontrado.", updateUser});
   });

// Peticiòn DELETE a users (mediante su ID)
app.delete(URL_BASE + 'users/:id',
     function(req, res){
       console.log("DELETE /techu/v1/users/:id");
       let idBuscar = req.params.id - 1;
       for(i = 0; i < usersFiles.length; i++) {
         console.log(usersFiles[i].id_user);
         if(usersFiles[i].id_user == idBuscar) {
            usersFiles.splice(idBuscar, 1);
            res.status(200).send({"msg" : "Usuario eliminado correctamente.", idBuscar});
         }
       }
       res.status(404).send({"msg" : "Usuario no encontrado.", idBuscar});
});

app.delete(URL_BASE + 'users1/:id',
  function(req, res){
    console.log("PUT /techu/v1/users/:id");
    let idBuscar = req.params.id - 1;
    let user = usersFiles[idBuscar];
    let respuesta, status;
    if(user == undefined){
      respuesta = {"msg":"Usuario no existe"};
      status = 404;
    }else{
      usersFiles.splice(idBuscar, 1);
      respuesta = {"msg" : "Usuario eliminado correctamente.", idBuscar};
      status = 200;
    }
    res.status(status).send(respuesta);
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
   console.log("POST /techu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   for(us of usersFiles) {
     if(us.email == user) {
       if(us.password == pass) {
         us.logged = true;
         writeUserDataToFile(usersFiles);
         console.log("Login correcto!");
         response.status(201).send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
       } else {
         console.log("Login incorrecto!");
         response.status(404).send({"msg" : "Login incorrecto!."});
       }
     }
  }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'user.json'.");
     }
   }); }

   // LOGOUT - users.json
app.post(URL_BASE + 'logout',
    function(request, response) {
      console.log("POST /techu/v1/logout");
      var userId = request.body.id_user;
      for(us of usersFiles) {
        if(us.id_user == userId) {
          if(us.logged) {
            delete us.logged; // borramos propiedad 'logged'
            writeUserDataToFile(usersFiles);
            console.log("Logout correcto!");
            response.status(201).send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
          } else {
            console.log("Logout incorrecto!.");
            response.status(404).send({"msg" : "Logout incorrecto."});
          }
        }  us.logged = true
      }
});

//Devolver la cantidad de usuario registrados
app.get(URL_BASE + 'total_users',
    function(request, response){
      let tam = usersFiles.length;
      console.log('Numero de usuarios: ' + tam);
      response.status(200).send({"num_usuarios " : tam});
});
