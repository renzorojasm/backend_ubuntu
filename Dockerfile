# Imagen Docker Base inicial
FROM node:latest

# Crear el directorio (carpeta de trabajo) del contenedor Docker
WORKDIR /docker-apitechu

# Copiar archivos del proyecto en el directorio de trabajo de Docker
ADD . /docker-apitechu


# Instalar las dependencias del proyecto para que la aplicacion funciones(las de prod)
# RUN permite ejecutar el comando de NPM para ejecutar las dependencias
# RUN npm install --only=production

#Puerto donde exponemos el contenedor (el mismo que usamos en la API)
EXPOSE 3000

#Comando para lanzar la app
CMD ["npm", "run", "prod"]
